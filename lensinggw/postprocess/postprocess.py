import numpy as np
import matplotlib as mpl
import matplotlib.colors as mcolors
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import os 
import sys
from lenstronomy.LensModel.lens_model import LensModel                                                                                                 
                  
def plot_images(output_folder,
                file_name,
                source_pos_x,
                source_pos_y,
                lens_model_list,
                kwargs_lens_list,
                ImgRA            = None,
                ImgDEC           = None,
                Mu               = None,
                Td               = None,
                MacroImgRA       = None,
                MacroImgDEC      = None,
                MacroMu          = None,
                MacroTd          = None,
                compute_window_x = 10**(-9),
                compute_window_y = 10**(-9),
                center_x         = 0,
                center_y         = 0,
                xmin             = -5*10**(-10),
                ymin             = -4*10**(-10),
                Npixels          = 10**3,
                how_left         = -(0.15*10**(-10)-2*10**(-10)),
                how_right        = 0.15*10**(-10),
                how_up           = 0.2e-11,
                how_down         = 0.7*10**(-10),
                mag_map          = False,
                diff             = None,
                **kwargs):  
     
    """
    Plots the images with magnifications, time delays, caustics and critical curves
    
    :param output_folder: output path. If *None*, the plot is displayed and not saved. If a string, the plot is saved and not displayed
    :type output_folder: None or string
    :param file_name: plot name, with extension. If *None*, *images.pdf* is used
    :type file_name: None or string
    :param source_pos_x: source right ascension (arbitrary units)
    :type source_pos_x: float
    :param source_pos_y: source declination (arbitrary units)
    :type source_pos_y: float
    :param lens_model_list: names of the lens profiles to be considered for the lens model
    :type lens_model_list: list of strings
    :param kwargs_lens_list: keyword arguments of the lens parameters matching each lens profile in *lens_model_list*
    :type kwargs_lens_list: list of dictionaries
    :param ImgRA: Images right ascensions (arbitrary units). If *None*, images are not plotted. *Optional*, default is *None*
    :type ImgRA: None or indexable object
    :param ImgDEC: Images declinations (arbitrary units). If *None*, images are not plotted. *Optional*, default is *None*
    :type ImgDEC: None or indexable object
    :param Mu: Images magnifications. If *None*, labels are not displayed. *Optional*, default is *None*
    :type Mu: None or indexable object
    :param Td: Images time delays in seconds. If *None*, labels are not displayed. *Optional*, default is *None*
    :type Td: None or indexable object
    :param MacroImgRA: Macroimages right ascensions (arbitrary units). If *None*, macroimages are not plotted. *Optional*, default is *None*
    :type MacroImgRA: None or indexable object
    :param MacroImgDEC: Macroimages declinations (arbitrary units). If *None*, macroimages are not plotted. *Optional*, default is *None*
    :type MacroImgDEC: None or indexable object
    :param MacroMu: Macroimages magnifications. If *None*, labels are not displayed. *Optional*, default is *None*
    :type MacroMu: None or indexable object
    :param MacroTd: Macroimages time delays in days. If *None*, labels are not displayed. *Optional*, default is *None*
    :type MacroTd: None or indexable object
    :param compute_window_x: horizontal size of the window (arbitrary units), points outside of if will not be plotted. *Optional*, default is :math:`10^{-9}`
    :type compute_window_x: float
    :param compute_window_y: vertical size of the window (arbitrary units), points outside of if will not be plotted. *Optional*, default is :math:`10^{-9}`
    :type compute_window_y: float
    :param center_x: window center right ascension (arbitrary units). *Optional*, default is :math:`0`
    :type center_x: float
    :param center_y: window center declination (arbitrary units). *Optional*, default is :math:`0`
    :type center_y: float
    :param Npixels: number of pixels of the image. *Optional*, default is :math:`10^3`
    :type Npixels: int
    :param how_left: leftward displacement between images and their labels (arbitrary units). *Optional*, default is :math:`1.85\\cdot10^{-10}`
    :type how_left: float
    :param how_right: rightward displacement between images and their labels (arbitrary units). *Optional*, default is :math:`0.15\\cdot10^{-10}`
    :type how_right: float
    :param how_up: upward displacement between images and their labels (arbitrary units). *Optional*, default is :math:`0.2\\cdot10^{-10}`
    :type how_up: float
    :param how_down: downward displacement between images and their labels (arbitrary units). *Optional*, default is :math:`0.7\\cdot10^{-10}`
    :type how_down: float
    :param mag_map: plots magnifications in a color-coded map. *Optional*, default is *False*
    :type mag_map: bool
    :param diff: If set, computes the deflection as a finite numerical differential of the lensing
         potential. This differential is only applicable in the single lensing plane where the form of the lensing
         potential is analytically known
    :type diff: None or float
    :param kwargs: specifies plotting options and plots additional quantities
    :param kwargs: dict
    
    :keyword rcParams: :math:`\\texttt{Matplotlib rcParams}` to use in place of the default ones. *Optional*, if not specified default options are used
    :kwtype rcParams: dict
    :keyword Macro_img_color: color of macroimages markers. *Optional*, default is *dodgerblue*
    :kwtype Macro_img_color: :math:`\\texttt{Matplotlib}` color string 
    :keyword Macro_img_symbol: symbol of macroimages markers. *Optional*, default is *.*
    :kwtype Macro_img_symbol: :math:`\\texttt{Matplotlib}` marker string
    :keyword img_color: color of images markers. *Optional*, default is *black*
    :kwtype img_color: :math:`\\texttt{Matplotlib}` color string
    :keyword img_symbol: symbol of images markers. *Optional*, default is *+*
    :kwtype img_symbol: :math:`\\texttt{Matplotlib}` marker string
    :keyword source_color: color of the source marker. *Optional*, default is *red*
    :kwtype source_color: :math:`\\texttt{Matplotlib}` color string 
    :keyword source_symbol: symbol of the source marker. *Optional*, default is *.*
    :kwtype source_symbol: :math:`\\texttt{Matplotlib}` marker string
    :keyword critical_color: color of the critical curves. *Optional*, default is *black*
    :kwtype critical_color: :math:`\\texttt{Matplotlib}` color string 
    :keyword caustics_color: color of the caustics. *Optional*, default is *red*
    :kwtype caustics_color: :math:`\\texttt{Matplotlib}` color string 
    :keyword title: plot title, *optional*. If not set, no title is displayed
    :kwtype title: None or string
    :keyword xlabel: :math:`x` axis label, *optional*. If not set, no label is displayed
    :kwtype xlabel: None or string
    :keyword ylabel: :math:`y` axis label, *optional*. If not set, no label is displayed
    :kwtype ylabel: None or string
    
    Further Keyword Arguments to plot additional quantities
        - **additional_pt_RAs** (*list of lists*) -- right ascensions of the additional datasets to plot out (arbitrary units), *optional*
        - **additional_pt_DECs** (*list of lists*) -- declinations of the additional datasets to plot out (arbitrary units), *optional*
        - **additional_pt_symbols** (*list of* :math:`\\texttt{Matplotlib}` *marker strings*) -- markers of the additional datasets. *Optional*, default is *.*
        - **additional_pt_colors** (*list of* :math:`\\texttt{Matplotlib}` *color strings*) -- colors of the additional datasets. *Optional*, default is *black*
        - **additional_pt_linestyles** (*list of* :math:`\\texttt{Matplotlib}` *linestyle strings*) -- line styles of the additional datasets. *Optional*, by default datasets are considered points instead of lines
        - **additional_pt_labels** (*list of strings*) -- labels of the additional datasets. *Optional*, by default labels are not displayed
    
    :returns: no return
    """
    
    ratio = 0.0 

    # default plotting options
    fig_width_pt  = 3*246.0                                          # get this from LaTeX using \showthe\columnwidth
    inches_per_pt = 1.0/72.27                                        # convert pt to inch
    golden_mean   = ratio if ratio != 0.0 else (np.sqrt(5)-1.0)/2.0  # aesthetic ratio
    fig_width     = fig_width_pt*inches_per_pt                       # width in inches
    fig_height    = fig_width*golden_mean                            # height in inches
    fig_size      = [fig_width,fig_height]

    params = {'axes.labelsize': 22,
              'font.family': 'DejaVu Sans',
              'font.serif': 'Computer Modern Raman',
              'font.size': 20,
              'legend.fontsize': 18,
              'xtick.labelsize': 22,
              'ytick.labelsize': 22,
              'axes.grid' : True,
              'text.usetex': True,
              'savefig.dpi' : 100,
              'lines.markersize' : 14, 
              'axes.formatter.useoffset': False,
              'figure.figsize': fig_size}
    
    # update rcParams with the user defined ones
    try:
        usr_params = kwargs['rcParams']
        for key in usr_params.keys():
            usr_value = usr_params[key]
            params.update({key: usr_value})
    except:
        pass

    mpl.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}'] #for \text command
    mpl.rcParams.update(params)
    
    # parse user defined plotting options
    Macro_color   = kwargs.get('Macro_img_color','dodgerblue')
    Macro_symbol  = kwargs.get('Macro_img_symbol','.')
    img_color      = kwargs.get('img_color','black')
    img_symbol     = kwargs.get('img_symbol','+')
    source_color   = kwargs.get('source_color','red')
    source_symbol  = kwargs.get('source_symbol','.')
    critical_color = kwargs.get('critical_color','black')
    caustics_color = kwargs.get('caustics_color','red')
    title          = kwargs.get('title',None)
    xlabel         = kwargs.get('xlabel',None)
    ylabel         = kwargs.get('ylabel', None)
                                
    # create the output folder if it doesn't already exist
    if output_folder is not None:
        os.system("mkdir -p {0}".format(output_folder))
    
    # instantiate the lens model
    lens_model = LensModel(lens_model_list=lens_model_list)
    
    # compute the caustics and critical curves of the complete model
    from lenstronomy.LensModel.lens_model_extensions import LensModelExtensions

    # maximum resolution => minimum window size
    compute_windowPix   = np.min([compute_window_x, compute_window_y])
    grid_scale          = compute_windowPix/Npixels 
    
    # maximum extension => maximum window size
    compute_window      = np.max([compute_window_x, compute_window_y])  
    
    lensModelExtensions = LensModelExtensions(lens_model)
    ra_crit_list, dec_crit_list,\
    ra_caustic_list, dec_caustic_list = lensModelExtensions.critical_curve_caustics(kwargs_lens_list, compute_window=compute_window, grid_scale=grid_scale)   


    # plot the image positions etc
    plt.plot(source_pos_x, source_pos_y, color=source_color, marker=source_symbol, linestyle='', label=r'$\mathrm{Source}$')
    
    # parse additional datasets, if given and plot them out
    try:
        ras  = kwargs['additional_pt_RAs']
        decs = kwargs['additional_pt_DECs']
        
        try:
            symbols = kwargs['additional_pt_symbols']
        except:
            symbols = ['.' for i in range(len(ras))]
        try:
            colors = kwargs['additional_pt_colors']
        except:
            colors = ['black' for i in range(len(ras))]
        try:
            linestyles = kwargs['additional_pt_linestyles']
        except:
            linestyles = [' ' for i in range(len(ras))]
        try:
            labels = kwargs['additional_pt_labels'] 
        except:
            labels = None
        
        for i in range(len(ras)):
            ra        = ras[i]
            dec       = decs[i]
            symbol    = symbols[i]
            color     = colors[i]
            linestyle = linestyles[i]

            if labels is not None:
                plt.plot(ra, dec, marker=symbol, linestyle=linestyle, color=color, label=labels[i])
            else:
                plt.plot(ra, dec, marker=symbol, linestyle=linestyle, color=color)
    except:
        pass
    
    # Macrourbed images
    if MacroImgRA is not None and MacroImgDEC is not None:
        if len(MacroImgRA)>1:
            plt.plot(MacroImgRA, MacroImgDEC, marker=Macro_symbol, linestyle='', color=Macro_color, label=r'$\mathrm{Macroimages}$')
        else:
            plt.plot(MacroImgRA, MacroImgDEC, marker=Macro_symbol, linestyle='', color=Macro_color, label=r'$\mathrm{Macroimage}$')
        
        # labels  
        if MacroMu is not None:
            if isinstance(MacroMu, int) or isinstance(MacroMu, float):
                MacroMu     = [MacroMu]
                MacroImgRA  = [MacroImgRA]
                MacroImgDEC = [MacroImgDEC]
            for i in range(len(MacroMu)):
                MacroMag =  r'$\mathrm{\mu\,=\,}$'+str(round(np.abs(MacroMu[i]),2))
                if MacroImgRA[i]<center_x:
                    x1 = MacroImgRA[i]-how_left
                else: 
                    x1 = MacroImgRA[i]+how_right
                y1 = MacroImgDEC[i]+how_up
                y2 = MacroImgDEC[i]-how_down
                plt.annotate(MacroMag, xy=(x1, y1))
                
        if MacroTd is not None:
            if isinstance(MacroTd, int) or isinstance(MacroTd, float):
                MacroTd = [MacroTd]
            for i in range(len(MacroTd)):
                MacroTimeDelay = r'$\mathrm{t_d\,=\,}$'+str(round(MacroTd[i],1))+r'$\mathrm{\, d}$'
                if MacroImgRA[i]<center_x:
                    x1 = MacroImgRA[i]-how_left
                else: 
                    x1 = MacroImgRA[i]+how_right
                y1 = MacroImgDEC[i]+how_up
                y2 = MacroImgDEC[i]-how_down
                plt.annotate(MacroTimeDelay, xy=(x1, y2))

    # images of the complete model
    if ImgRA is not None and ImgDEC is not None:
        plt.plot(ImgRA, ImgDEC, marker=img_symbol, mew=1.2, linestyle='', color=img_color, label=r'$\mathrm{Images}$')
    
        # labels 
        if Mu is not None:
            if isinstance(Mu, int) or isinstance(Mu, float):
                Mu = [Mu]
            for i in range(len(Mu)): 
                mag =  r'$\mathrm{\mu\,=\,}$'+str(round(np.abs(Mu[i]),2)) 
                if ImgRA[i]<center_x:
                    x1 = ImgRA[i]-how_left
                else: 
                    x1 = ImgRA[i]+how_right
                y1 = ImgDEC[i]+how_up
                y2 = ImgDEC[i]-how_down
                plt.annotate(mag, xy=(x1, y1))
                
        if Td is not None:
            if isinstance(Td, int) or isinstance(Td, float):
                Td = [Td]
            for i in range(len(Td)):    
                timeDelay = r'$\mathrm{t_d\,=\,}$'+str(round(Td[i]*1.e3,2))+r'$\mathrm{\, ms}$'
                if ImgRA[i]<center_x:
                        x1 = ImgRA[i]-how_left
                else: 
                    x1 = ImgRA[i]+how_right
                y1 = ImgDEC[i]+how_up
                y2 = ImgDEC[i]-how_down      
                plt.annotate(timeDelay, xy=(x1, y2))
        
    # caustics and critical curves
    for i in range(len(ra_crit_list)):
        if i==0:
            plt.plot(ra_crit_list[i], dec_crit_list[i], color=critical_color, linewidth=0.7, label=r'$\mathrm{Critical \,curves}$')
        else:
            plt.plot(ra_crit_list[i], dec_crit_list[i], color=critical_color, linewidth=0.7)
    for i in range(len(ra_caustic_list)):
        if i==0:
            plt.plot(ra_caustic_list[i], dec_caustic_list[i], color=caustics_color, linewidth=0.7, label=r'$\mathrm{Caustics}$')
        else:
            plt.plot(ra_caustic_list[i], dec_caustic_list[i], color=caustics_color, linewidth=0.7)
    
    xmin = center_x-compute_window_x/2. 
    xmax = compute_window_x+xmin
    ymin = center_y-compute_window_y/2.
    ymax = compute_window_y+ymin
    
    # plot out the magnifications
    if mag_map:
        from lensinggw.utils.utils import magnifications
        
        num_points_x     = int(compute_window_x/grid_scale)
        num_points_y     = int(compute_window_y/grid_scale)
        num_points       = np.max([num_points_x,num_points_y])
        x,y              = np.linspace(xmin, xmax, num_points), np.linspace(ymin, ymax,num_points)
        x,y              = np.meshgrid(x, y)
        z                = np.zeros((len(x),len(y)))

        for i in range(len(x)):
            for j in range(len(y)):
                mu     = np.abs(magnifications( x[i,j], y[i,j], lens_model_list, kwargs_lens_list, diff=diff))
                z[i,j] = np.abs(np.log10(np.abs(mu)))
        from matplotlib import cm
        plt.contourf(x, y, z, cmap=cm.get_cmap('gray_r'))
        cbar = plt.colorbar()
        cbar.ax.set_ylabel(r'$\mathrm{Magnification \, |\log_{10} \, \mu|}$')
        
    # limits and labels
    plt.xlim(xmin,xmax)
    plt.ylim(ymin,ymax)
    if title is not None:
        plt.title(title)
    if xlabel is not None:
        plt.xlabel(xlabel)
    if ylabel is not None:
        plt.ylabel(ylabel)
    plt.legend(loc='upper left', prop={'size':15})
    plt.tight_layout()
    
    # save or display the plot
    if output_folder is None:
        plt.show()
    else:
        if file_name is None: 
            file_name = 'images.pdf'
        plt.savefig(output_folder+file_name, bbox_inches='tight')
    plt.close()

def plot_gw(output_folder,
            file_name, 
            x_data,
            y_data,
            scale = 'loglog',
            **kwargs):  
            
    """ 
    Plots GW signals
    
    :param output_folder: output path. If *None*, the plot is displayed and not saved. If a string, the plot is saved and not displayed
    :type output_folder: None or string
    :param file_name: plot name, with extension. If *None*, *GWsignals.pdf* is used
    :type file_name: None or string
    :param x_data: datasets to plot out, :math:`x` coordinates
    :type x_data: list of lists
    :param y_data: datasets to plot out, :math:`y` coordinates
    :type y_data: list of lists
    :param scale: axis scale. Must be *loglog*, *semilogx* or *linear*. *Optional*, default is *loglog* 
    :type scale: string    
    :param kwargs: specifies plotting options
    :type kwargs: dict
    
    :keyword rcParams: :math:`\\texttt{Matplotlib rcParams}` to use in place of the default ones. *Optional*, if not specified default options are used
    :kwtype rcParams: dict
    :keyword title: plot title, *optional*. If not set, no title is displayed
    :kwtype title: None or string
    :keyword xlabel: :math:`x` axis label, *optional*. If not set, no label is displayed
    :kwtype xlabel: None or string
    :keyword ylabel: :math:`y` axis label, *optional*. If not set, no label is displayed
    :kwtype ylabel: None or string
    :keyword xlim: :math:`x` axis limits, *optional*. If not set, limits are set by :math:`\\texttt{Matplotlib}`
    :kwtype xlim: None or :math:`2d` list
    :keyword ylim: :math:`y` axis limits, *optional*. If not set, limits are set by :math:`\\texttt{Matplotlib}`
    :kwtype ylim: None or :math:`2d` list
    :keyword linestyles: :math:`\\texttt{Matplotlib}` *linestyle* strings. If the list has one element, the same line style will be used for all the datasets. *Optional*, default is a continuous line style
    :kwtype linestyles: list of strings
    :keyword colors: :math:`\\texttt{Matplotlib}` *color* strings. If the list has one element, the same color will be used for all the datasets. *Optional*, default is a linear colormap 
    :kwtype colors: list of strings
    :keyword labels: labels for each dataset. *Optional*, by default labels are not displayed
    :kwtype labels: list of strings
    
    :returns: no return
    """
                  
    # default plotting options
    ratio         = 0.0
    fig_width_pt  = 3*246.0                                          # get this from LaTeX using \showthe\columnwidth
    inches_per_pt = 1.0/72.27                                        # convert pt to inch
    golden_mean   = ratio if ratio != 0.0 else (np.sqrt(5)-1.0)/2.0  # aesthetic ratio
    fig_width     = fig_width_pt*inches_per_pt                       # width in inches
    fig_height    = fig_width*golden_mean                            # height in inches
    fig_size      = [fig_width,fig_height]

    params = {'axes.labelsize': 32,
              'font.family': 'DejaVu Sans',
              'font.serif': 'Computer Modern Raman',
              'font.size': 32,
              'legend.fontsize': 28,
              'xtick.labelsize': 32,
              'ytick.labelsize': 32,
              'axes.grid' : True,
              'text.usetex': True,
              'savefig.dpi' : 100,
              'lines.markersize' : 14, 
              'axes.formatter.useoffset': False,
              'figure.figsize': fig_size}
    
    # update rcParams with the user defined ones
    try:
        usr_params = kwargs['rcParams']
        for key in usr_params.keys():
            usr_value = usr_params[key]
            params.update({key: usr_value})
    except:
        pass
        
    mpl.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}'] #for \text command
    mpl.rcParams.update(params)
    
    # parse title, axis labels and limits if given
    title  = kwargs.get('title',None)
    xlabel = kwargs.get('xlabel',None)
    ylabel = kwargs.get('ylabel', None)
    xlim   = kwargs.get('xlim', None)
    ylim   = kwargs.get('ylim', None)
    
    # create the output folder if it doesn't already exist
    if output_folder is not None:
        os.system("mkdir -p {0}".format(output_folder))
    
    # initialize plot line styles, colors and labels
    try:
        linestyles  = kwargs['linestyles']
        nlinestyles = len(linestyles)
        if nlinestyles == 1:
            linestyles = [linestyles[0] for i in range(len(x_data))]
    except:
        linestyles = ['-' for i in range(len(x_data))]
        
    try:
        colors  = kwargs['colors']
        ncolors = len(colors)
        if ncolors == 1:
            colors = [colors[0] for i in range(len(x_data))]
    except:
        ncolors = 0
        
    if ncolors == 0:
        colors      = []
        color_range = np.linspace(6.,3.5,len(x_data)*2+1)
        colorparams = color_range
        colormap    = cm.gray
        normalize   = mcolors.Normalize(vmin=np.min(colorparams), vmax=np.max(colorparams))
        for i in range(len(x_data)):
            color = colormap(normalize(color_range[i])-0.2)
            colors.append(color)
            
    try:
        labels  = kwargs['labels']
        nlabels = len(labels)
    except:
        nlabels = 0    
    
    # plot the datasets
    for i in range(len(x_data)):
        x         = x_data[i]
        y         = y_data[i]
        color     = colors[i]
        linestyle = linestyles[i]
        
        if nlabels == 0:
                if scale == 'loglog':
                   plt.loglog(x, y, color=color, linestyle=linestyle)
                elif scale == 'semilogx':
                    plt.semilogx(x, y, color=color, linestyle=linestyle)
                elif scale == 'linear':
                    plt.plot(x, y, color=color, linestyle=linestyle)    
                else:
                    sys.stderr.write('\n\nUnsuppotred scale. Must be \'loglog\' or \'semilogx\' or \'linear\'\n')
                    sys.stderr.write('Aborting\n')
                    exit(-1)
        else:
            label = labels[i]
            if scale == 'loglog':
               plt.loglog(x, y, color=color, linestyle=linestyle, label=label)
            elif scale == 'semilogx':
                plt.semilogx(x, y, color=color, linestyle=linestyle, label=label)
            elif scale == 'linear':
                plt.plot(x, y, color=color, linestyle=linestyle, label=label)    
            else:
                sys.stderr.write('\n\nUnsuppotred scale. Must be \'loglog\' or \'semilogx\' or \'linear\'\n')
                sys.stderr.write('Aborting\n')
                exit(-1)
    
    if title is not None:
        plt.title(title)
    if xlabel is not None:
        plt.xlabel(xlabel)
    if ylabel is not None:
        plt.ylabel(ylabel)
    if xlim is not None:
        plt.xlim(xlim)
    if ylim is not None:
        plt.ylim(ylim)
    if nlabels != 0:
        plt.legend(loc='best', prop={'size':15})
    plt.tight_layout()
    
    # save or display the plot
    if output_folder is None:
        plt.show()
    else:
        if file_name is None: 
            file_name = 'GWsignals.pdf'
        plt.savefig(output_folder+file_name, bbox_inches='tight')
    plt.close()

import numpy as np
import lalsimulation as lalsim
import lal
from lal import ComputeDetAMResponse, GreenwichMeanSiderealTime, TimeDelayFromEarthCenter, LIGOTimeGPS
from lensinggw.amplification_factor.amplification_factor import geometricalOpticsMagnification
import lensinggw.constants.constants as const
from scipy.signal import tukey
from scipy.interpolate import interp1d

Msun = const.M_sun
Mpc  = const.Mpc

class waveform_model(object):
    """
    Class to calculate GW signals at the source and their projections into the detectors
    
    :param x: binary parameters
    :type x: dict
    :param waveform_input: waveform specifications
    :type waveform_input: dict
    :param detectors: power spectral densities of the detectors
    :type detectors: dict
        
    Example
    -------
    WaveformModel = waveform_model(*injection_parameters*, *waveform_parameters*, *detectors*) 
    """

    def __init__(self, x, waveform_input, detectors):
    
        self.d           = x['distance']     
        self.m1          = x['m1']
        self.m2          = x['m2']
        self.inclination = x['inclination']
        self.phi0        = x['phi0']
        
        self.wave_flags    = None
        self.non_GR_params = None
        
        self.df          = waveform_input['df'] 
        self.flow        = waveform_input['flow']
        self.fhigh       = waveform_input['fhigh']
        self.fref        = waveform_input['fref']
        sampling_rate    = waveform_input['sampling-rate'] 
        T                = 1./self.df
        N                = int(sampling_rate*T)  
        self.frequencies = self.df*np.arange(0,N/2.+1)
        self.kmin        = int(self.flow/self.df)
        self.kmax        = int(self.fhigh/self.df)+1
        
        
        # set the LAL alias for the approximant and define its domain
        self.approx = lalsim.SimInspiralGetApproximantFromString(waveform_input['approximant'])
        if lalsim.SimInspiralImplementedFDApproximants(self.approx): self.domain = 'frequency'
        elif lalsim.SimInspiralImplementedTDApproximants(self.approx): self.domain = 'time'
           
        # time domain specifications
        if self.domain == 'time':          
            self.epoch          = waveform_input['starttime']
            self.tc             = waveform_input['trigtime']
            self.dt             = 1.0/sampling_rate
            self.segment_length = int(sampling_rate*T)
            
            # set up the window for the FFT
            padding         = 0.4/T
            self.window     = tukey(self.segment_length,padding)
            self.windowNorm = self.segment_length/np.sum(self.window**2)

        # general specifications
        self.amp_order   = waveform_input['amp_order']
        self.phase_order = waveform_input['phase_order']
        self.fref        = float(waveform_input['fref'])

        # spins
        self.spin1x = x['spin1x']
        self.spin1y = x['spin1y']
        self.spin1z = x['spin1z']
        self.spin2x = x['spin2x']
        self.spin2y = x['spin2y']
        self.spin2z = x['spin2z']
       
        # check for tidal parameters
        try:
            self.lambda1 = x['lambda1']
        except:
            self.lambda1 = 0.0
        try:
            self.lambda2 = x['lambda2']
        except:
            self.lambda2 = 0.0

        self.LALpars = lal.CreateDict()
        lalsim.SimInspiralWaveformParamsInsertTidalLambda1(self.LALpars,self.lambda1)
        lalsim.SimInspiralWaveformParamsInsertTidalLambda2(self.LALpars,self.lambda2)
        lalsim.SimInspiralWaveformParamsInsertPNAmplitudeOrder(self.LALpars,int(self.amp_order))
        lalsim.SimInspiralWaveformParamsInsertPNPhaseOrder(self.LALpars,int(self.phase_order))
        
        # detectors
        self.detectors    = detectors
        self.lal_detector = {}
        self.det_response = {}
        self.det_loc      = {}
        
        for name in self.detectors.keys():
            self.lal_detector.update({name: lal.cached_detector_by_prefix[name]})
            self.det_response.update({name: self.lal_detector[name].response})
            self.det_loc.update({name: self.lal_detector[name].location})

        # parameters for the projection into the detectors
        self.ra    = x['ra']
        self.dec   = x['dec']
        self.psi   = x['psi']
        self.tc    = waveform_input['trigtime']
        self.Epoch = waveform_input['starttime']

    def plain_template(self):
        """
        Computes unlensed polarizations
        
        :return: unlensed :math:`f, \, \\tilde{h}_+(f), \, \\tilde{h}_{\\times}(f)` 
        :rtype: array, array, array   
        """
    
        # LAL waveform generator for frequency domain signals
        if self.domain == 'frequency':
            hptilde, hctilde = lalsim.SimInspiralChooseFDWaveform(self.m1*Msun,
                                                                  self.m2*Msun,
                                                                  self.spin1x, self.spin1y, self.spin1z,
                                                                  self.spin2x, self.spin2y, self.spin2z,
                                                                  self.d*Mpc,
                                                                  self.inclination,
                                                                  self.phi0,
                                                                  0, #longAscNodes
                                                                  0, #eccentricity
                                                                  0, #meanPerAno
                                                                  self.df,
                                                                  self.flow,
                                                                  self.fhigh,
                                                                  self.fref,
                                                                  self.LALpars,
                                                                  self.approx)


            # unpack data
            hptilde = hptilde.data.data
            hctilde = hctilde.data.data
            self.hpt,self.hct = hptilde.astype(np.complex128,copy=False),hctilde.astype(np.complex128,copy=False)
            
        # LAL waveform generator for time domain signals
        elif self.domain == 'time':
            hp, hc = lalsim.SimInspiralChooseTDWaveform(self.m1*Msun,
                                                        self.m2*Msun,
                                                        self.spin1x, self.spin1y, self.spin1z,
                                                        self.spin2x, self.spin2y, self.spin2z,
                                                        self.d*Mpc,
                                                        self.inclination,
                                                        self.phi0,
                                                        0, #longAscNodes
                                                        0, #eccentricity
                                                        0, #meanPerAno
                                                        self.dt,
                                                        self.flow,
                                                        self.fref,
                                                        self.LALpars,
                                                        self.approx)

            # align with the desired time of coalescence
            hp, hc = resize_time_series(np.column_stack((hp.data.data, hc.data.data)), self.segment_length, self.dt, self.epoch, self.tc)
            
            # window the signal
            hp  *= self.window
            hc  *= self.window
            
            # perform the FFT, multiply by dt to get the correct units
            hptilde  = np.fft.fft(hp)*self.windowNorm*self.dt 
            hctilde  = np.fft.fft(hc)*self.windowNorm*self.dt
            self.hpt,self.hct  = hptilde.astype(np.complex128,copy=False), hctilde.astype(np.complex128,copy=False)

        return self.frequencies[self.kmin:self.kmax], self.hpt[self.kmin:self.kmax], self.hct[self.kmin:self.kmax]
        
    def strain(self):    
        """
        Computes unlensed strains in the requested detectors
        
        :return: unlensed :math:`s = \{\'det_i \, \': s^{det_i}\}` where :math:`s^{det} =\\tilde{h}_+(f)F^{det}_+ + \\tilde{h}_{\\times}(f)F^{det}_{\\times}` is the strain in the detector :math:`det`
        :rtype: dict
        """
    
        strain_dict = {}
        
        for name in self.detectors.keys():
        
            # locate each detector
            det_response = self.det_response[name]
            det_loc      = self.det_loc[name]
            
            # project the source polarizations onto it
            s_temp = self.project(det_response, det_loc)
            strain_dict.update({name: s_temp})
            
        return strain_dict
        
    def project(self, det_response, det_loc, lensed = False, **kwargs):
        """
        Projects lensed/unlensed polarizations into the given detector
        
        :param det_response: response of a *LALDetector class* instance, lal.Detector.response
        :type det_response: array
        :param det_loc: location of a *LALDetector class* instance, lal.Detector.location
        :type det_loc: array
        :param lensed: specifies whether to compute a lensed or unlensed signal
        :type lensed: bool
        :param kwargs: keyword arguments to specify for the computation of lensed signals if *lensed* is *True*
        :type kwargs: dict
        
        :keyword Img_ra: images right ascensions (arbitrary units)
        :kwtype Img_ra: indexable object
        :keyword Img_dec: images declinations (arbitrary units)
        :kwtype Img_dec: indexable object
        :keyword source_pos_x: source right ascension (arbitrary units)
        :kwtype source_pos_x: float
        :keyword source_pos_y: source declination (arbitrary units)
        :kwtype source_pos_y: float
        :keyword zL: lens redshift
        :kwtype zL: float
        :keyword zS: source redshift
        :kwtype zS: float
        :keyword lens_model_list: names of the profiles to be considered for the lens model
        :kwtype lens_model_list: list of strings
        :keyword kwargs_lens_list: keyword arguments of the lens parameters matching each lens profile in *lens_model_list*
        :kwtype kwargs_lens_list: list of dictionaries
        :keyword diff: step for numerical differentiation, *optional*. Only needed for potentials that require numerical differentiation. If not specified, analytical differentiation is assumed
        :kwtype diff: float
        :keyword scaled: specifies if the input is given in arbitrary units, *optional*. If not specified, the input is assumed to be in radians
        :kwtype scaled: bool
        :keyword scale_factor: scale factor, *optional*. Used to account for the proper conversion factor in the time delays when coordinates are given in arbitrary units, as per :math:`x_{a.u.} = x_{radians}/scale\_factor`. Only considered when *scaled* is *True*
        :kwtype scale_factor: float
        :keyword cosmo: cosmology used to compute angular diameter distances, *optional*. If not specified, a :math:`\\mathrm{FlatLambdaCDM}` instance with :math:`H_0=69.7, \Omega_0=0.306, T_{cmb0}=2.725` is considered
        :kwtype cosmo: instance of the astropy cosmology class
            
        :return: lensed/unlensed :math:`s =\\tilde{h}_+(f)F_+ + \\tilde{h}_{\\times}(f)F_{\\times}`
        :rtype: array
        """

        # get the unlensed source polarizations
        freqs, hptilde, hctilde = self.plain_template()
        
        # if lensed, compute the lensed ones
        if lensed:
            Img_ra           = kwargs['Img_ra']
            Img_dec          = kwargs['Img_dec']
            source_pos_x     = kwargs['source_pos_x']
            source_pos_y     = kwargs['source_pos_y']
            zL               = kwargs['zL']
            zS               = kwargs['zS']
            lens_model_list  = kwargs['lens_model_list']
            kwargs_lens_list = kwargs['kwargs_lens_list']
            diff             = kwargs['diff']
            scaled           = kwargs['scaled']
            scale_factor     = kwargs['scale_factor']
            cosmo            = kwargs['cosmo']

            # magnification factor
            F = geometricalOpticsMagnification(freqs,
                                               Img_ra,Img_dec,
                                               source_pos_x,source_pos_y,
                                               zL,zS,
                                               lens_model_list,
                                               kwargs_lens_list,
                                               diff         = diff,
                                               scaled       = scaled,
                                               scale_factor = scale_factor,
                                               cosmo        = cosmo)

            # lensed polarizations
            hptilde *= F
            hctilde *= F
            
        # time shift of each detector and antenna patterns
        tc        = self.tc
        ra        = self.ra
        dec       = self.dec
        psi       = self.psi
        
        tgps      = LIGOTimeGPS(tc)
        gmst      = GreenwichMeanSiderealTime(tgps)
        fp,fc     = ComputeDetAMResponse(det_response, ra, dec, psi, gmst)
        timedelay = TimeDelayFromEarthCenter(det_loc, ra, dec, tgps)
        timeShift = timedelay
        if self.domain == 'frequency':
            timeShift += tc - self.Epoch

        # return the projection into the detector
        return (fp*hptilde+fc*hctilde)*np.exp(-1j*2.0*np.pi*timeShift*freqs)
    
    def SNR(self, lensed = False, **kwargs):
        """
        Computes lensed/unlensed signal-to-noise ratios in the detectors
        
        :param lensed: specifies whether to compute a lensed or unlensed signals
        :type lensed: bool
        :param kwargs: keyword arguments to specify for the computation of lensed signals if *lensed* is *True*
        :type kwargs: dict
        
        :keyword Img_ra: images right ascensions (arbitrary units)
        :kwtype Img_ra: indexable object
        :keyword Img_dec: images declinations (arbitrary units)
        :kwtype Img_dec: indexable object
        :keyword source_pos_x: source right ascension (arbitrary units)
        :kwtype source_pos_x: float
        :keyword source_pos_y: source declination (arbitrary units)
        :kwtype source_pos_y: float
        :keyword zL: lens redshift
        :kwtype zL: float
        :keyword zS: source redshift
        :kwtype zS: float
        :keyword lens_model_list: names of the lens profiles to be considered for the lens model
        :kwtype lens_model_list: list of strings
        :keyword kwargs_lens_list: keyword arguments of the lens parameters matching each lens profile *lens_model_list*
        :kwtype kwargs_lens_list: list of dictionaries
        :keyword diff: step for numerical differentiation, *optional*. Only needed for potentials that require numerical differentiation. If not specified, analytical differentiation is assumed
        :kwtype diff: float
        :keyword scaled: specifies if the input is given in arbitrary units, *optional*. If not specified, the input is assumed to be in radians
        :kwtype scaled: bool
        :keyword scale_factor: scale factor, *optional*. Used to account for the proper conversion factor in the time delays when coordinates are given in arbitrary units, as per :math:`x_{a.u.} = x_{radians}/scale\_factor`. Only considered when *scaled* is *True*
        :kwtype scale_factor: float
        :keyword cosmo: cosmology used to compute angular diameter distances, *optional*. If not specified, a :math:`\\mathrm{FlatLambdaCDM}` instance with :math:`H_0=69.7, \Omega_0=0.306, T_{cmb0}=2.725` is considered
        :kwtype cosmo: instance of the astropy cosmology class
        
        :return: lensed/unlensed signal-to-noise-ratios :math:`SNR = \{\'det_i \, \': SNR^{det_i}\}` of the lensed strains in each detector. Stores the :math:`SNR` of the global network under the key :math:`\'network\'`
        :rtype: dict
        """
     
        self.SNR_dict = {}
        
        for name in self.detectors.keys():
            
            # get the power spectral densities for each detector and interpolate them over the relevan frequency range
            psd_file = self.detectors[name]       
            f, psd   = np.loadtxt(psd_file, unpack=True)
            psd_int  = interp1d(f, psd, bounds_error=False, fill_value=np.inf)
            psd      = psd_int(self.frequencies)
            PowerSpectralDensity = psd[self.kmin:self.kmax]
            
            # LAL detector response and location
            det_response = self.det_response[name]
            det_loc      = self.det_loc[name]
            
            # project the polarizations onto each detector
            template = self.project(det_response, det_loc, lensed, **kwargs)
            
            # signal-to-noise ratio
            SNR_temp = np.sqrt(4.0*self.df*np.sum(np.conj(template)*template/PowerSpectralDensity).real)
            self.SNR_dict.update({name: SNR_temp})
        
        # network signal-to-noise ratio        
        SNR_net = np.sqrt(np.sum(([s**2 for (key,s) in self.SNR_dict.items()])))
        self.SNR_dict.update({'network': SNR_net})
        
        return self.SNR_dict
                       
def resize_time_series(inarr, N, dt, starttime, desiredtc):
    """
    Resizes the components of *inarr* to length *N* and aligns their peaks to the desired time of coalescence *desiredtc* in the segment
    
    :param inarr: two-dimentional array containing the time domain polarizations to resize and align, :math:`inarr=\mathrm{array}(h_+(t),h_{\\times}(t))`
    :type inarr: two-dimentional array
    :param N: required length of the resized array
    :type N: int
    :param dt: required time interval between the samples of the resized array
    :type dt: float
    :param starttime: time of the first sample of :math:`h_+(t)` and :math:`h_{\\times}(t)`
    :type starttime: float
    :param desiredtc: required peak time of the resized array
    :type desiredtc: float
    
    :return: resized :math:`h_+(t)` and :math:`h_{\\times}(t)`, with peak time at *desiredtc* 
    :rtype: array, array
    """
    
    # length of the array before resizing
    waveLength = inarr.shape[0]

    # sample of the desired tc 
    tcSample = int(np.floor((desiredtc-starttime)/dt))
    
    # and the corresponding time
    injTc = starttime + tcSample*dt

    # sample at which tc is located in the array before resizing, using the square amplitude as reference
    waveTcSample = np.argmax(inarr[:,0]**2+inarr[:,1]**2)
    
    # procedure: copy a chunk of the actual array into a buffer of the length N, aligning the actual tc to the desired one
    # and cutting the extra samples
    
    # determine the first index of the chunk to copy: it's the very first sample of the array if it must be moved ahead to 
    # perform the alignment (copy all the samples before the peak, but shifted ahead in the buffer), it's the number of
    # samples to cut at the beginning if it must be moved back (move the array back by excluding the extra samples at the beginning)
    if (tcSample >= waveTcSample):
        waveStartIndex = 0
    else:
        waveStartIndex = waveTcSample - tcSample

    # samples after the peak before resizing
    wavePostTc = waveLength - waveTcSample
    
    # determine the first index of the buffer where it will be copied: it's the amount of samples by which the array must to be moved
    # ahead in the first case (copy all the samples before the peak, but shifted ahead in the buffer), it's the very beginning of the
    # buffer in the second case (move back the array by cutting the extra initial samples)
    if tcSample >= waveTcSample:
        bufstartindex =  tcSample - waveTcSample
    else:
        bufstartindex = 0
        
    # determine the last index of the chunk to copy: copy all the samples after the peak if the new length of the waveform after
    # the alignment, wavePostTc + tcSample, is shorter than the buffer; cut the extra samples after the peak otherwise
    #
    # determine the last index of the buffer where it will be copied: it's the new length of the waveform wavePostTc + tcSample if
    # it is shorter than the buffer, it's the last index of the buffer otherwise (cut the extra samples)
    if (wavePostTc + tcSample <= N):
        bufendindex  = wavePostTc + tcSample
        waveEndIndex = waveLength
    else:
        bufendindex  = N
        waveEndIndex = waveStartIndex + bufendindex - bufstartindex

    # buffer length
    bufWaveLength = bufendindex - bufstartindex;

    # allocate the arrays of zeros which work as a buffer
    hp = np.zeros(N,dtype = np.float64)
    hc = np.zeros(N,dtype = np.float64)
    
    # copy the waveform over
    hp[bufstartindex:bufstartindex+bufWaveLength] = inarr[waveStartIndex:waveEndIndex,0]
    hc[bufstartindex:bufstartindex+bufWaveLength] = inarr[waveStartIndex:waveEndIndex,1]

    return hp,hc
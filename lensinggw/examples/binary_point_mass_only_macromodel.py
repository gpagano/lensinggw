# This script illustrates how to use lensingGW's OnlyMacro feature to solve the macromodel of a binary point mass, assuming radians 

import numpy as np

# coordinates, first define them in scaled units [x (radians) /thetaE_tot]
y0,y1 = 0.1,0.5*np.sqrt(3)   
l0,l1 = 0.5,0  

# redshifts                                                                                                                  
zS = 2.0 
zL = 0.5  

# masses 
mL1  = 100                                                                   
mL2  = 100  
mtot = mL1+mL2  

# convert to radians
from lensinggw.utils.utils import param_processing

thetaE1 = param_processing(zL, zS, mL1)                                                                                                                              
thetaE2 = param_processing(zL, zS, mL2)                                                                                                                              
thetaE  = param_processing(zL, zS, mtot)    

beta0,beta1 = y0*thetaE,y1*thetaE                                                 
eta10,eta11 = l0*thetaE,l1*thetaE                                                                                                
eta20,eta21 = -l0*thetaE,l1*thetaE  

# lens model
lens_model_list     = ['POINT_MASS', 'POINT_MASS'] 
kwargs_point_mass_1 = {'center_x': eta10,'center_y': eta11, 'theta_E': thetaE1} 
kwargs_point_mass_2 = {'center_x': eta20,'center_y': eta21, 'theta_E': thetaE2} 
kwargs_lens_list    = [kwargs_point_mass_1, kwargs_point_mass_2]   

# solve for the macromodel only with the OnlyMacro option: indicate both lenses as macromodel
from lensinggw.solver.images import microimages

solver_kwargs = {'OnlyMacro'        : True,
                 'SearchWindowMacro': 4*thetaE1,
                 'MacroIndex'       : [0,1]} 
                 
MacroImg_ra, MacroImg_dec, pixel_width  = microimages(source_pos_x    = beta0,
                                                      source_pos_y    = beta1,
                                                      lens_model_list = lens_model_list,
                                                      kwargs_lens     = kwargs_lens_list,
                                                      **solver_kwargs)                                                          
                                                                       
# time delays, magnifications, Morse indices and amplification factor
from lensinggw.utils.utils import TimeDelay, magnifications, getMinMaxSaddle
from lensinggw.amplification_factor.amplification_factor import geometricalOpticsMagnification

tds = TimeDelay(MacroImg_ra, MacroImg_dec,
                beta0, beta1,
                zL, zS,
                lens_model_list, kwargs_lens_list)                
mus = magnifications(MacroImg_ra, MacroImg_dec, lens_model_list, kwargs_lens_list)
ns  = getMinMaxSaddle(MacroImg_ra, MacroImg_dec, lens_model_list, kwargs_lens_list) 
                
print('Time delays (seconds): ', tds)
print('magnifications: ',  mus)
print('Morse indices: ',ns)
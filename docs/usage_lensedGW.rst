Get the lensed gravitational waves
==================================
* `Example 6 - Get the lensed gravitational waves <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/lensed_waveform.py>`__

| In addition to the `waveform configuration file and to the noise curves <https://gpagano.gitlab.io/lensinggw/usage.html#simulate-gravitational-waves>`_,
  the simulation of lensed signals requires to specify a lens model and a set of images, from which the amplification factor is computed.
| Images can be found as described in the `solver section <https://gpagano.gitlab.io/lensinggw/usage.html#solve-the-lens-model>`_, where the
  lens model initialization is also shown. 
| Assuming that a *lens_model_list* and a *kwargs_lens_list* have been defined and that the *Img_ra* and *Img_dec* arrays
  store the images' right ascensions and declinations, one can modify the `previous example <https://gpagano.gitlab.io/lensinggw/usage.html#get-the-unlensed-gravitational-waves>`__ as follows to get the lensed quantities
 
.. code-block:: python
 
        # compute the lensed waveform polarizations, strains in the requested detectors and their frequencies
        waveform_model.lensed_gw(Img_ra, Img_dec,
                                 source_pos_x = 1.39e-11, source_pos_y = 1.20e-10,
                                 zL = 0.5 , zS = 2.0,
                                 lens_model_list,
                                 kwargs_lens_list)

.. code-block:: python

        # and their signal-to-noise-ratios
        waveform_model.lensed_snr(Img_ra, Img_dec,
                                  source_pos_x = 1.39e-11, source_pos_y = 1.20e-10,
                                  zL = 0.5 , zS = 2.0,
                                  lens_model_list,
                                  kwargs_lens_list)
 
**Arbitrary units** 

If the input is in `arbitrary units <https://gpagano.gitlab.io/lensinggw/usage.html#arbitrary-units>`_, the scaling has to be notified
to the lensed waveform routines by enabling the **scaled** flag and indicating the scale factor. The previous lines can be
modified to use :math:`\alpha = \theta_{E_1}` as follows


.. code-block:: python
 
        # compute the lensed waveform polarizations, strains in the requested detectors and their frequencies
        waveform_model.lensed_gw(Img_ra, Img_dec,
                                  .
                                  .
                                 scaled = True,
                                 scale_factor = thetaE1)

.. code-block:: python

        # and their signal-to-noise-ratios
        waveform_model.lensed_snr(Img_ra, Img_dec,
                                   .
                                   .
                                  scaled = True,
                                  scale_factor = thetaE1)

where omitted lines are unchanged.

lensingGW package
=================

| ``lensingGW`` implements the algorithm presented in `Pagano et al. 2020 <https://www.aanda.org/articles/aa/full_html/2020/11/aa38730-20/aa38730-20.html>`_ for solving the lens equation and an infrastructure for gravitational-wave signals.
| The algorithm is designed to resolve strongly lensed images and microimages simultaneously, such as the images resulting from hundreds of microlenses embedded in galaxies and galaxy clusters.
| The gravitational-wave infrastructure allows to predict the resulting gravitational-wave signals in ground-based detectors.

| The lens model is handled through a `forked version of Lenstronomy <https://github.com/gipagano/lenstronomy>`_ -- original version
 `here <https://github.com/sibirrer/lenstronomy>`_ (`Birrer & Amara 2018 <https://ui.adsabs.harvard.edu/abs/2018PDU....22..189B/abstract>`_). ``lensingGW`` is 
  therefore compatible with any lens profile implemented therein. 

Subpackages
-----------

.. toctree::

    lensinggw.amplification_factor
    lensinggw.postprocess
    lensinggw.solver
    lensinggw.utils
    lensinggw.waveform


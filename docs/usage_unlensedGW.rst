Get the unlensed gravitational waves
====================================
* `Example 5 - Get the unlensed gravitational waves <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/unlensed_waveform.py>`_

To generate unlensed waveforms, ``lensingGW``'s signal model has to be initialized through the configuration file

.. code-block:: python

        from lensinggw.waveform.waveform import gw_signal

        # read the waveform parameters
        config_file = 'ini_files/waveform_config.ini'

        # instantiate the waveform model
        waveform_model = gw_signal(config_file)

Polarizations, strains and their frequencies can be accessed through ``lensingGW``'s unlensed routines

.. code-block:: python

        # compute the unlensed waveform polarizations, strains in the requested detectors and their frequencies
        freqs, hp_tilde, hc_tilde, strain_dict = waveform_model.unlensed_gw()

        # and their signal-to-noise-ratios
        SNR_dict = waveform_model.unlensed_snr()

The *strain* and *SNR* dictionaries allow to access information of each detector individually

.. code-block:: python

        # access an unlensed strain
        sH1 = strain_dict['H1']